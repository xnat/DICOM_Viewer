# DICOM-Data-Viewer

This project is a simple program which opens DICOM-Files and shows its fields.

## Files

* decom_viewer.py

## Prerequisites

* python
* pydicom, pygtk (pip install -r prerequisites.txt)

## How to use

* start dicom_viewer.py
* click file-open, choose a dicom-file, click on open
* you can also supply a file to be opened as a command line argument
