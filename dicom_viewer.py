import gtk

class DvApp(gtk.Window):

    def open_file(self, filename):
        if self.treeView.load_file(filename):
            from os.path import abspath
            self.statusbar.push(0, abspath(filename))

    def open_file_dialog(self, widget, data=None):
        dialog = gtk.FileChooserDialog(title=None,action=gtk.FILE_CHOOSER_ACTION_OPEN, buttons=(gtk.STOCK_CANCEL,gtk.RESPONSE_CANCEL,gtk.STOCK_OPEN,gtk.RESPONSE_OK))
        try:
            response = dialog.run()
            if response == gtk.RESPONSE_OK:
                self.open_file(dialog.get_filename())
            elif response == gtk.RESPONSE_CANCEL:
                print 'Closed, no files selected'
        finally:
            dialog.destroy()

    def __init__(self):
        super(DvApp, self).__init__()

        self.set_size_request(800, 500)
        self.set_position(gtk.WIN_POS_CENTER)

        self.connect("destroy", gtk.main_quit)
        self.set_title("DICOM Viewer")

        vbox = gtk.VBox(False, 8)

        menuBar = gtk.MenuBar()

        menu = gtk.Menu()
        filemenu = gtk.MenuItem("File")
        filemenu.set_submenu(menu)

        mi_open = gtk.MenuItem("open")
        mi_open.connect("activate", self.open_file_dialog)
        menu.append(mi_open)

        menuBar.append(filemenu)
        vbox.pack_start(menuBar, False, False, 0)

        sw = gtk.ScrolledWindow()
        sw.set_shadow_type(gtk.SHADOW_ETCHED_IN)
        sw.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        vbox.pack_start(sw, True, True, 0)

        self.treeView = DicomTreeView()
        self.treeView.set_rules_hint(True)

        sw.add(self.treeView)

        self.statusbar = gtk.Statusbar()

        vbox.pack_start(self.statusbar, False, False, 0)

        self.add(vbox)
        self.show_all()

        import sys
        if len(sys.argv)>1:
            self.open_file(sys.argv[1])

class DicomTreeView(gtk.TreeView):

    def __init__(self):
        super(DicomTreeView, self).__init__()
        self.create_columns()

    @staticmethod
    def create_model(dicom_file):
        import dicom
        store = gtk.TreeStore(str, str, str, str)
        dicoms = dicom.read_file(dicom_file)

        def append_to_store(store, data, parent=None):
            from dicom.dataelem import DataElement
            from dicom.dataset import Dataset
            if not type(data) == DataElement:
                raise ValueError("Data-structure erroneous")
            if isinstance(data.value, list) and data.value and isinstance(data.value[0], Dataset):
                t1 = store.append(parent,(data.tag, data.name, data.VR, '%d Elements' % len(data.value)))
                for count, v in enumerate(data.value):
                    t2 = store.append(t1, ('Element %d' % count, '', '', ''))
                    for d in v:
                        append_to_store(store, d, t2)
            else:
                store.append(parent,(data.tag, data.name, data.VR, data.repval))

        for data in dicoms:
            append_to_store(store, data)

        return store

    def create_columns(self):

        rendererText = gtk.CellRendererText()
        column = gtk.TreeViewColumn("Tag", rendererText, text=0)
        column.set_sort_column_id(0)
        self.append_column(column)

        rendererText = gtk.CellRendererText()
        column = gtk.TreeViewColumn("Attribute Name", rendererText, text=1)
        column.set_sort_column_id(1)
        self.append_column(column)

        rendererText = gtk.CellRendererText()
        column = gtk.TreeViewColumn("VR", rendererText, text=2)
        column.set_sort_column_id(2)
        self.append_column(column)

        rendererText = gtk.CellRendererText()
        column = gtk.TreeViewColumn("Value", rendererText, text=3)
        column.set_sort_column_id(3)
        self.append_column(column)

    def load_file(self, dicom_file):
        from dicom.filereader import InvalidDicomError
        try:
            self.set_model(self.create_model(dicom_file))
            model = self.get_model()
            return True
        except InvalidDicomError:
            print "Wrong File Format"
        except:
            raise

DvApp()
gtk.main()
